#include <iostream>
#include <cmath>

int Interface();

void ExampleA(int n);
void ExampleB(int n);
void ExampleC(int n);
void ExampleD(int n);
void ExampleE(int n);
void ExampleF(int n);
void ExampleG(int n);
void ExampleH(int n);
void ExampleI(int n);
void ExampleJ(int n);

int Factorial(int i);

int main()
{
	Interface();

	return 0;
}

int Interface()
{
	std::cout << "Hello! ";

	while (true)
	{
		std::cout << "Which example do you want to count? Name the letter from a to j and write it in lowercase: ";

		char Example_number;

		std::cin >> Example_number;

		std::cout << "\nPlease! Give me value of N: ";

		int n;

		std::cin >> n;

		switch (Example_number)
		{
			case 'a':
				ExampleA(n);
				break;
			case 'b':
				ExampleB(n);
				break;
			case 'c':
				ExampleC(n);
				break;
			case 'd':
				ExampleD(n);
				break;
			case 'e':
				ExampleE(n);
				break;
			case 'f':
				ExampleF(n);
				break;
			case 'g':
				ExampleG(n);
				break;
			case 'h':
				ExampleH(n);
				break;
			case 'i':
				ExampleI(n);
				break;
			case 'j':
				ExampleJ(n);
				break;
			default:
				std::cout << "Sorry! Try again.\n";
				system("pause");
				system("cls");
				break;
		}

		std::cout << "do you want to continue ? y/n" << std::endl;

		char YN;

		std::cin >> YN;

		if (YN == 'N' or YN == 'n')
			return 0;

		system("cls");

		std::cout << "Okay! Let's continue! ";
	}
}

/*
Я вижу, что у меня повторений строчек 103-110 в каждой функции примера. Я хочу спать, оставим так :) 
Если упрощать код, нужно тело одной функции написать один раз, а менять только используемую для
Подсчёта функцию.
*/

void ExampleA(int n)
{
	system("cls");

	double sum{ 0 };
	
	for (float i = 1; i <= n; i++)
	{
		sum += 1 / i;
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleB(int n)
{
	system("cls");

	double sum{ 1 };

	for (float i = 1; i <= n; i++)
	{
		sum *= (1 + 1 / i / i);
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleC(int n)
{
	std::cout << "Please! Give me value of K: ";

	int k;

	std::cin >> k;

	system("cls");

	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / pow( i, k );
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleD(int n)
{
	system("cls");

	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += pow( -1, i+1 ) / ( i * ( i + 1 ) );
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleE(int n)
{
	system("cls");

	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / (2 * i + 1) / (2 * i + 1);
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleF(int n)
{
	system("cls");

	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += pow( -1, i ) / ( 2 * i + 1 );
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleG(int n) // Начиная от примера G и дальше дз не доделал. Можно не смотреть. Извиняюсь, позже доделаю.
{
	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / i;
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleH(int n)
{
	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / i;
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleI(int n)
{
	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / i;
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}

void ExampleJ(int n)
{
	double sum{ 0 };

	for (float i = 1; i <= n; i++)
	{
		sum += 1 / i;
	}

	std::cout << "Result of the amount for your order: " << sum << std::endl;
}
